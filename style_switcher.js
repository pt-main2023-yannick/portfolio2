const styleSwitcherToggler = document.querySelector('.style_switcher_toggler');
styleSwitcherToggler.addEventListener("click",()=>{
    document.querySelector('.style_switcher').classList.toggle("open");
})

window.addEventListener("scroll", ()=>{
    if(document.querySelector('.style_switcher').classList.contains('open')){
        document.querySelector('.style_switcher').classList.remove("open");
    }
})

const alternativeStyle = document.querySelectorAll(".alternate_style");
function setActiveStyle(color) 
{
    alternativeStyle.forEach((style) => {
        if (color === style.getAttribute('title')) {
            style.removeAttribute('disabled')
        }else{
            style.setAttribute('disabled', 'true')
        }
    }

    )
}

const dayNight = document.querySelector('.day_night');
dayNight.addEventListener('click', () =>{
    dayNight.querySelector('i').classList.toggle('bx-sun');
    dayNight.querySelector('i').classList.toggle('bx-moon');
    document.body.classList.toggle('dark');
})
window.addEventListener('load', ()=>{
    if (document.body.classList.contains('dark')) {
        dayNight.querySelector('i').classList.add('bx-sun');
    }
    else{
        dayNight.querySelector('i').classList.add('bx-moon');
       
    }
})

